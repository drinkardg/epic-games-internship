Hello Epic Games! Thank you so much for taking the time to look through my portfolio.

As a heads up, some of the art is very small and I recommend opening them up in an editing software to see the full detail of the sprites!
In an effort to not make this file huge, I only chose some of the best art I have made since I started doing art myself about a year ago.

The .exe for the playable module is in the zipped file. I have a Game Design Document attached for this playable module that gives an overview 
of what the game should eventually be and details some of the story. 

That being said, I will also attach documents for other game ideas I have come up with (and have started working on) in the past!

Again, thank you!